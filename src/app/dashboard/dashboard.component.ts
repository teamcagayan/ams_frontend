import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthApiService } from '../ams-web-api/auth-api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _authService:  AuthApiService, private _router:  Router) { }

  ngOnInit(): void {
  }

  public logout() {
    this._authService.removeToken();
    this._router.navigateByUrl('login');
  }
}
