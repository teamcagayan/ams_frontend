import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from "./dashboard/dashboard.component";

import { AuthApiService } from "./ams-web-api/auth-api.service";
import { AuthGuardService } from "./ams-web-api/auth-guard.service";

const routes: Routes = [
  { path:  'login', component:  LoginComponent },  
  { 
    path:  'dashboard', 
    component:  DashboardComponent, 
    canActivate:  [AuthGuardService] 
  },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' }    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:  [
    AuthApiService,
    AuthGuardService
  ]
})
export class AppRoutingModule { }