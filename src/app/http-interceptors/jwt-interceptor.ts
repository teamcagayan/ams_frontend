import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { AuthApiService } from "../ams-web-api/auth-api.service";
import { TokenModel } from '../ams-web-api/token-model';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private _authService:  AuthApiService) {}

  intercept(
    req:  HttpRequest<any>, 
    next:  HttpHandler
  ):  Observable<HttpEvent<any>> {
    let token:  TokenModel = this._authService.getToken();

    if (token.access) {
      const cloned = req.clone({ headers:  req.headers.set("Authorization", "JWT " + 
        token.access) });

      return next.handle(cloned);
    } else {
      return next.handle(req);
    }   
  }
}