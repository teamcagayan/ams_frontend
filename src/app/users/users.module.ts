import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { LoginComponent } from "../login/login.component";

import { AuthApiService } from "../ams-web-api/auth-api.service";


@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    FormsModule    
  ],
  providers:  [AuthApiService]
})
export class UsersModule { }
