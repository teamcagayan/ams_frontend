export class TokenModel {
  public access:  string;
  public refresh:  string;
  public errors:  string;

  constructor() {
    this.access = '';
    this.refresh = '';
    this.errors = '';
  }
}