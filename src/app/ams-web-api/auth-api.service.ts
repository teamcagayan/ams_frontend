import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { catchError, shareReplay, map } from "rxjs/operators";
import { Observable, throwError } from 'rxjs';

import { TokenModel } from "./token-model";

@Injectable()
export class AuthApiService {
  //private url = "http://localhost:8000/auth";
  private url = "https://amcs-backend.herokuapp.com/auth";
  private httpOptions:  any;
  
  constructor(private http:  HttpClient) { 
    this.httpOptions = {
      headers:  new HttpHeaders({
        'Content-Type':'application/json'
      })
    };    
  }
  
  private handleError(error:  HttpErrorResponse) {
    let msg:  string;

    if (error.error instanceof ErrorEvent) {
      msg = 'Client side error:  ' + error.message;
    } else {
      if (error.status == 401)
        msg = 'Invalid username/password!!!';
      else
        msg = 'Server side error:  ' + error.message;
    }    
    
    return throwError(msg)
  }

  private isVerified(token:  TokenModel):  boolean {
    let result:  boolean = false;

    if (token.access)
      result = true;
    else
      result = false;
    
    return result;
  }

  public getToken():  TokenModel {
    let token:  TokenModel;

    if (localStorage.getItem('token')) {
      token = JSON.parse(localStorage.getItem('token'));
    } else {
      token = new TokenModel();
    }

    return token;
  }

  public storeToken(token:  TokenModel) {
    localStorage.setItem('token', JSON.stringify(token));
  }

  public removeToken() {
    localStorage.setItem('token',JSON.stringify(new TokenModel()));
  }
    
  public authenticate(postData:  any):  Observable<TokenModel> {
    return this.http.post<TokenModel>(
      this.url + '/jwt/create/', 
      postData
    ).pipe(      
      catchError(err => this.handleError(err)), 
      shareReplay()
    );
  }

  public verify(token:  TokenModel):  Observable<boolean> {
    return this.http.post(
      this.url + '/jwt/verify/', 
      {"token": token.access}
    ).pipe(    
      map(() => true),
      catchError(err => this.handleError(err)),
      shareReplay()
    );
  }
}
