import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from 
  "@angular/router";
import { Observable } from "rxjs";
import { catchError, shareReplay } from 'rxjs/operators';

import { AuthApiService } from "./auth-api.service";
import { TokenModel } from './token-model';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private authService:  AuthApiService, private router:  Router) { }

  canActivate(
    route:  ActivatedRouteSnapshot, 
    state:  RouterStateSnapshot
  ):  Observable<boolean> {
    let token:  TokenModel = this.authService.getToken();

    return this.authService.verify(token).pipe(
      catchError(() => this.router.navigateByUrl('login')),
      shareReplay()
    );
  }
}
