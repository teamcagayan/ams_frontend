import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { AuthApiService } from "../ams-web-api/auth-api.service";

import { LoginModel } from "./login-model";
import { TokenModel } from "../ams-web-api/token-model";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user:  LoginModel;
  token:  TokenModel;
  
  constructor(private authService:  AuthApiService, private router:  Router) {}

  ngOnInit(): void {
    this.user = new LoginModel();
    this.token = new TokenModel();
  } 

  public setToken(tokenData:  TokenModel) {
    this.authService.storeToken(tokenData);
    this.token = tokenData;
  }

  public resetToken() {    
    this.token = new TokenModel();
  }

  public login() {
    this.resetToken();    
    this.authService.authenticate({
      'username':  this.user.username, 
      'password':  this.user.password
    }).subscribe(
      data => { this.setToken(data); this.redirect() }, 
      err => { this.token.errors = err }
    )
  }

  public redirect() {
    this.router.navigateByUrl('');
  }
}
