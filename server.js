const express = require('express');

const app = express();

app.use(express.static('./dist/ams-frontend'));
app.get('/*', function(req, res) {
  res.sendFile('index.html', {root:  'dist/ams-frontend/'});
});
app.listen(process.env.PORT || 4200);